package com.flying.fish.formwork.service;

import com.flying.fish.formwork.base.BaseService;
import com.flying.fish.formwork.dao.SecureIpDao;
import com.flying.fish.formwork.entity.SecureIp;
import org.springframework.stereotype.Service;

/**
 * @Description IP管理业务操作类
 * @Author jianglong
 * @Date 2020/05/28
 * @Version V1.0
 */
@Service
public class SecureIpService extends BaseService<SecureIp, String, SecureIpDao> {
}
